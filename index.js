/*
1. In the S31 folder, create an activity folder and an activity.js file inside of it.
2. Copy the questions provided by your instructor into the activity.js file.
3. The questions are as follows:
- What directive is used by Node.js in loading the modules it needs?
- What Node.js module contains a method for server creation?
- What is the method of the http object responsible for creating a server using Node.js?
- What method of the response object allows us to set status codes and content types?
- Where will console.log() output its contents when run in Node.js?
- What property of the request object contains the address's endpoint?
4. Create an index.js file inside of the activity folder.
5. Import the http module using the required directive.
6. Create a variable port and assign it with the value of 3000.
7. Create a server using the createServer method that will listen in to the port provided above.
8. Console log in the terminal a message when the server is successfully running.
9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
10. Access the login route to test if it’s working as intended.
11. Create a condition for any other routes that will return an error message.
12. Access any other route to test if it’s working as intended.
13. Create a git repository named S31.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.
*/

const http = require("http");

const port = 3000;

const server = http.createServer(function(request, response){

	if(request.url == "/"){
		response.writeHead(200,{"Content-Type" : "text/plain"});
		response.end("Welcome to the web page");
	}


	else if (request.url == "/login"){
		response.writeHead(200,{"Content-Type" : "text/plain"});
		response.end("Welcome to the login page");;
	}

	else if (request.url == "/register"){
		response.writeHead(404,{"Content-Type" : "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}

	else{ 
         response.writeHead(404,{"Content-Type" : "text/plain"});
         response.end("Page not available");
	}

});

server.listen(port);
console.log (`Server is now accessible at localhost: ${port}`); 
